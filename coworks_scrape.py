from openpyxl import load_workbook, Workbook
import sys
import requests
import re
from bs4 import BeautifulSoup, SoupStrainer
from lxml import html

fb_re = re.compile(r'https://www.facebook.com/[^\?\'\"<]+')
fb_page_email_re = re.compile(r'[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+')
fb_page_phone_re = re.compile(r'\+*\d{1,7} \d{1,7} \d{1,7} *\d{1,7} *\d{1,7} *\d{1,7}')
email_re = re.compile(r'[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+')
phone_re = re.compile(r'\+*\d{0,3} *\(?\+*[0-9]{1,3}\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})? ?(\w{1,10}\s?\d{1,6})?')
fb_exclude = ['.php', '/tr', 'plugins/', 'search/', '2008/fbml', 'events/', 'posts/', 'media/']


def read_from_workbook(filename):
	"""
	Function which reads data from workbook
	column1 - name, column2 - url, column3 - country
	:param filename: name of the input file, string
	:return: list of dictionaries refferring to rows
	"""

	try:
		workbook = load_workbook(filename)
	except OSError as e:
		sys.exit("Impossible to open {} file.\nPlease, check if it exists in the current directory and its permissions.\n\nThe following error occured:{}".format(filename, e))

	first_sheet = workbook.get_sheet_names()[0]
	worksheet = workbook.get_sheet_by_name(first_sheet)

	if worksheet.max_row == 1:
		sys.exit("Empty sheet {}".format(first_sheet))

	coworks_list = []
	unique_urls = []

	for row in worksheet.iter_rows(min_row=2, max_row=worksheet.max_row+1):
		if row[1].value not in unique_urls:
			temp_dict = {'name':row[0].value, 'url':row[1].value, 'country':row[2].value}
			coworks_list.append(temp_dict)
			unique_urls.append(row[1].value)

	print('\n{} unique urls filtered out of {}\n'.format(len(unique_urls), worksheet.max_row - 2))

	return coworks_list


def write_to_workboook(filename, coworks):
	"""
	Function which converts list of dicts into list of lists
	and writes data with coworks into file
	:param filename: name of the output file, string
	:param coworks: list of dictionaries with data of one cowork
	:return: 0
	"""

	res_coworks = [['NAME', 'URL', 'STATUS_CODE', 'EMAIL', 'PHONE NUMBER', 'FACEBOOK', 'TWITTER', 'ADDRESS']]
	for i in range(len(coworks) - 1):
		if not 'address' in coworks[i]:
			coworks[i]['address'] = ''
		if not 'phone' in coworks[i]:
			coworks[i]['phone'] = ''
		if not 'email' in coworks[i]:
			coworks[i]['email'] = ''
		if not 'face_status_code' in coworks[i]:
			coworks[i]['face_status_code'] = ''
		if not 'twitter' in coworks[i]:
			coworks[i]['twitter'] = ''
		try:
			res_coworks.append([coworks[i]['name'], coworks[i]['url'], coworks[i]['status_code'],
								coworks[i]['email'], coworks[i]['phone'], coworks[i]['facebook'], coworks[i]['twitter'], coworks[i]['address']])

		except Exception:
			res_coworks.append([coworks[i]['name'], coworks[i]['url'], coworks[i]['exception']])

	book = Workbook()
	sheet = book.active

	for row in res_coworks:
		sheet.append(row)

	try:
		book.save(filename)
	except OSError as e:
		sys.exit("Impossible to write to {} file. Maybe, you forgot to close it?\n\nThe following error occured:{}".format(filename, e))

	return 0


def parse_facebook_response(cowork, facebook_url, facebook_primary_url = False):
	"""
	Function which parses responce of facebook page
	:param cowork: dictionary with data of one cowork
	:param facebook_url: string, facebook link
	:param facebook_primary_url: is set to True only if original url taken from excel file is facebook url
	:return: updated dictionary of cowork data
	"""

	facebook_url = facebook_url
	facebook_url += 'about/' if facebook_url.endswith('/') else '/about'
	if 'http' not in facebook_url:
		facebook_url = 'https://' + facebook_url
	#facebook_url = 'https://' + facebook_url
	try:
		response = requests.get(facebook_url, timeout = 25)
	except Exception as e:
		cowork['exception'] = 'Exception occured when connecting to facebook. Check the link'
		return cowork
	cowork['face_status_code'] = response.status_code
	if facebook_primary_url:
		cowork['facebook'] = 'The same as input link'
		cowork['status_code'] = response.status_code
	if not 'email' in cowork:
		try:
			cowork['email'] = fb_page_email_re.findall(response.text)[0].replace('&#064;', '@')
		except Exception:
			cowork['email'] = ''
	tree = html.fromstring(response.content)
	ph_num_and_email = tree.xpath('//div[@class="_50f4"]/text()')
	addr = tree.xpath('//span[@class="_50f4"]/text()')
	address = ''
	if 'address' not in cowork:
		for ad in addr:
			if '@' not in ad:
				address += ad
		cowork['address'] = address
	if 'email' not in cowork:
		for em in ph_num_and_email:
			temp_email = fb_page_email_re.findall(em)
			if temp_email:
				cowork['email'] = temp_email[0]
				break
	if 'phone' not in cowork:
		for ph in ph_num_and_email:
			temp_ph = fb_page_phone_re.findall(ph)
			if temp_ph:
				cowork['phone'] = temp_ph[0]
				break

	return (cowork)


def filter_out_facebooks(facebooks):
	"""
	Function which filters out incorrect facebook addresses
	:param facebooks: list of facebook urls found within all urls in the webpage
	:return: list of filtered facebooks
	"""

	goood_facebooks = []
	if len(facebooks) > 0:
		for facebook in facebooks:
			exclude_bool = False
			for exclude in fb_exclude:
				if exclude in facebook:
					exclude_bool = True
			if exclude_bool == False:
				if facebook not in goood_facebooks and len(facebook) > 10:
					goood_facebooks.append(facebook)
		set(goood_facebooks)
		return goood_facebooks
	else:
		return []


def get_email_from_contact_page(cowork, contact_page_url):
	"""
	Functions which checks contact page of the website for the email
	if it was not found on the main page
	:param cowork: dictionary with data of one cowork
	:param contact_page_url: url of contact page in cowork website
	:return: dictionary with populated data of cowork
	"""

	try:
		response = requests.get(contact_page_url, timeout=25)
	except Exception as e:
		return cowork
	emails = set([email.strip() for email in email_re.findall(response.text) if '.png' not in email and '.jpg' not in email and 'example' not in email])
	if len(emails) > 0:
		cowork['email'] = ', '.join(emails)
	return cowork


def parse_response(cowork, response_text):
	"""
	Function which gets all the needed fields from html page
	:param cowork: dictionary with data of one cowork
	:param response_text: html page content
	:return: dictionary with populated data of cowork
	"""

	facebooks = []
	links = BeautifulSoup(response_text,  "html.parser", parse_only=SoupStrainer('a'))
	for link in links:
		if link.has_attr('href'):
			if 'facebook.com' in link['href']:
				facebooks.append(link['href'])
			elif 'twitter.com' in link['href']:
				cowork['twitter'] = link['href']
			# https://garagebilk.de/lage-und-kontakt/index.html train with https://garagebilk.de
	emails = set([email.strip() for email in email_re.findall(response_text) if '.png' not in email and '.jpg' not in email and 'example' not in email])
	if len(emails) > 0:
		cowork['email'] = ', '.join(emails)
	facebooks = filter_out_facebooks(facebooks)
	cowork['facebook'] = ', '.join(facebooks)
	if facebooks:
		for facebook in facebooks:
			cowork = parse_facebook_response(cowork, facebook)
	if 'email' not in cowork:
		for link in links:
			if link.has_attr('href'):
				if 'contact' in link['href'] or 'kontakt' in link['href'] or 'touch' in link['href'] or 'connect' in link['href']:
					cowork = get_email_from_contact_page(cowork, link['href'])
	return cowork


def populate_from_urls(cowork):
	"""
	Function which is making get request to the cowork website,
	it checks link type (facebook/non-facebook), handles exceptions, triggers response parsing
	:param cowork: dictionary with data of one cowork
	:return: dictionary with populated data of cowork
	"""

	print('processing url {}'.format(cowork['url']))
	if 'http' not in cowork['url']:
		cowork['url'] = 'http://' + cowork['url']
	if 'facebook' in cowork['url']:
		cowork = parse_facebook_response(cowork, cowork['url'], True)
		return cowork
	try:
		response = requests.get(cowork['url'], timeout=25)
	except Exception as e:
		cowork['exception'] = 'Exception occured - max retries exceeded etc.'
		return cowork
	if response.status_code == 403:
		headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
		try:
			response = requests.get(cowork['url'], headers=headers)
		except Exception as e:
			cowork['exception'] = 'Exception occured - 403'
			if response.status_code == 403:
				return cowork
	cowork['status_code'] = response.status_code
	cowork = parse_response(cowork, response.text)
	return cowork


if __name__ == "__main__":
	input_filename = sys.argv[1]
	output_filename = sys.argv[2]
	write_to_workboook(output_filename, [])
	if 'xlsx' not in input_filename or 'xlsx' not in output_filename:
		sys.exit('Please, write correct extensions for input and output files. You can check README.txt file for an example')
	coworks = read_from_workbook(input_filename)
	to_be_processed = len(coworks)
	processed = 0
	for i in range(len(coworks)-1):
		coworks[i] = populate_from_urls(coworks[i])
		processed += 1
		if processed % 50 == 0:
			print('\nProcessed {} entries out of {}.\n'.format(processed, to_be_processed))
			write_to_workboook(output_filename, coworks[:i])

	write_to_workboook(output_filename, coworks)
