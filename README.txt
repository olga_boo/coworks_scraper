Tested with Python version 3.5 on Windows platform

Python set up:
Python can be installed from:
https://www.python.org/downloads/release/python-350/
download: Windows x86-64 executable installer
Add path to Python executable to the PATH system variable:
C:\Python35\

In the terminal:
1. cd to the project directory (coworks_scraper)

2. optionally, but desirable :
- virtualenv coworks_venv
- coworks_venv\Scripts\activate

3. in order to install all the requiremants:
pip install -r requirements.txt

4. make sure, that you have network connection

5. run the script:
python coworks_scrape.py <input_file_name> <output_file_name>
example:
python coworks_scrape.py coworks_sheet_2.xlsx coworks_res.xlsx
Please, don't forget to add extension to the files!!!
